﻿![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/884c69c7065c4ca58ce77cc4068d7cff.gif#pic_center =500x)

> 【作者主页】[Francek Chen](https://blog.csdn.net/Morse_Chen?type=blog)
> 【专栏介绍】[$⌈$Python机器学习$⌋$](https://blog.csdn.net/morse_chen/category_12652926.html?spm=1001.2014.3001.5482) 机器学习是一门人工智能的分支学科，通过算法和模型让计算机从数据中学习，进行模型训练和优化，做出预测、分类和决策支持。Python成为机器学习的首选语言，依赖于强大的开源库如Scikit-learn、TensorFlow和PyTorch。本专栏介绍机器学习的相关算法以及基于Python的算法实现。
> 【GitCode】数据集和相关资源保存在我的GitCode仓库[https://gitcode.com/Morse_Chen/Python_machine_learning](https://gitcode.com/Morse_Chen/Python_machine_learning)。


@[TOC](文章目录)

---
## 一、Scikit-learn概述
&emsp;&emsp;[Scikit-learn](https://scikit-learn.org.cn/)是基于NumPy、SciPy和Matplotlib的开源Python机器学习包，它封装了一系列数据预处理、机器学习算法、模型选择等工具，是数据分析师首选的机器学习工具包。
&emsp;&emsp;自2007年发布以来，scikit-learn已经成为Python重要的机器学习库了，scikit-learn简称sklearn，支持包括分类，回归，降维和聚类四大机器学习算法。还包括了特征提取，数据处理和模型评估三大模块。

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/33596182265040af8335d63fb358b0a5.png#pic_center =800x)
## 二、Scikit-learn主要用法
**符号标记**：

| 符号 | 意义 | 符号 | 意义 |
|--|--|--|--|
| `X_train` | 训练数据 | `y_train` | 训练集标签 |
| `X_test` | 测试数据 | `y_test` | 测试集标签 |
| `X` | 完整数据 | `y` | 数据标签 |

### （一）基本建模流程
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/0cd11bebc53440f5b6c6716ceac670a6.png#pic_center =600x)
**<font color=red>总体处理流程可以分为：加载数据集、数据预处理、数据集划分、模型估计器创建、模型拟合、模型性能评估</font>**

### （二）加载数据集

**1. 导入工具包**

```python
from sklearn import datasets, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
```

**2. 加载数据**

&emsp;&emsp;Scikit-learn支持以NumPy的arrays对象、Pandas对象、SciPy的稀疏矩阵及其他可转换为数值型arrays的数据结构作为其输入，前提是数据必须是数值型的。
&emsp;&emsp;`sklearn.datasets`模块提供了一系列加载和获取著名数据集如鸢尾花、波士顿房价、Olivetti人脸、MNIST数据集等的工具，也包括了一些toy data如S型数据等的生成工具。

```python
from sklearn.datasets import load_iris
iris = load_iris()
X = iris.data
y = iris.target
```

### （三）划分数据集

```python
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=12, stratify=y, test_size=0.3)
```

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/2f3200877d7a487cbb089333ff0d225e.png#pic_center =500x)
将完整数据集的70%作为训练集，30%作为测试集，并使得测试集和训练集中各类别数据的比例与原始数据集比例一致（stratify分层策略），另外可通过设置`shuffle=True`提前打乱数据。

### （四）数据预处理

**1. 数据变换**

使⽤Scikit-learn进行数据标准化：

```python
# 导入数据标准化工具包
from sklearn.preprocessing import StandardScaler
# 构建转换器实例
scaler = StandardScaler()
# 拟合及转换
scaler.fit_transform(X_train)
```

Z-Score标准化： $$x^*=\frac{x-\mu}{\sigma} \\[2ex]
\sigma^2=\frac{1}{m}\sum_{i=1}^m(x^{(i)}-\mu)^2，\mu=\frac{1}{m}\sum_{i=1}^mx^{(i)}$$ 处理后的数据均值为0，方差为1。

使用Scikit-learn进行数据变换：

- 最小最大标准化：`MinMaxScaler`
归一化（最大 - 最小规范化）：$$x^*=\frac{x-x_{\min}}{x_{\max}-x_{\min}}$$ 将数据映射到 $[0,1]$ 区间

- One-Hot编码：`OneHotEncoder`
- 归一化：`Normalizer`
- 二值化（单个特征转换）：`Binarizer`
- 标签编码：`LabelEncoder`
- 缺失值填补：`Imputer`
- 多项式特征生成：`PolynomialFeatures`

**2. 特征选择**

```python
from sklearn import feature_selection as fs
```

过滤式（Filter），保留得分排名前k的特征（top k方式）。

```python
fs.SelectKBest(score_func, k)
```

封装式（Wrap- per），结合交叉验证的递归特征消除法，自动选择最优特征个数。

```python
fs.RFECV(estimator, scoring=“r2”)
```

嵌入式（Embedded），从模型中自动选择特征，任何具有`coef_`或者`feature_importances_`的基模型都可以作为estimator参数传入。

```python
fs.SelectFromModel(estimator)
```

### （五）构建并拟合模型

**1. 监督学习算法——回归**

```python
from sklearn.linear_model import LinearRegression
# 构建模型实例
lr = LinearRegression(normalize=True)
# 训练模型
lr.fit(X_train, y_train)
# 作出预测
y_pred = lr.predict(X_test)
```
- LASSO：`linear_model.Lasso`
- Ridge：`linear_model.Ridge`
- ElasticNet：`linear_model.ElasticNet`
- 回归树：`tree.DecisionTreeRegressor`

**2. 监督学习算法——分类**

```python
from sklearn.tree import DecisionTreeClassifier
# 构建模型实例
clf = DecisionTreeClassifier(max_depth=5)
# 训练模型
clf.fit(X_train, y_train)
# 作出预测
y_pred = clf.predict(X_test)
y_prob = clf.predict_proba(X_test)
```
使用决策树分类算法解决二分类问题，`y_prob`为每个样本预测为“0”和“1”类的概率。

- 逻辑回归：`linear_model.LogisticRegression`
- 支持向量机：`svm.SVC`
- 朴素贝叶斯：`naive_bayes.GaussianNB`
- K近邻：`neighbors.NearestNeighbors`

**3. 监督学习算法——集成学习**

`sklearn.ensemble`模块包含了一系列基于集成思想的分类、回归和离群值检测方法。

```python
from sklearn.ensemble import RandomForestClassifier
# 构建模型实例
clf = RandomForestClassifier(n_estimators=20)
# 训练模型
clf.fit(X_train, y_train)
# 作出预测
y_pred = clf.predict(X_test)
y_prob = clf.predict_proba(X_test)
```
- AdaBoost（适应提升算法）:
`ensemble.AdaBoostClassifier`
`ensemble.AdaBoostRegressor`
- GBboost（梯度提升算法）：
`ensemble.GradientBoostingClassifier`
`ensemble.GradientBoostingRegressor`

**4. 无监督学习算法——k-means**

`sklearn.cluster`模块包含了一系列无监督聚类算法。

```python
from sklearn.cluster import KMeans
# 构建模型实例
kmeans = KMeans(n_clusters=3, random_state=0)
# 训练模型
kmeans.fit(X_train)
# 作出预测
kmeans.predict(X_test)
```

- DBSCAN：`cluster.DBSCAN`
- 层次聚类：`cluster.AgglomerativeClustering`
- 谱聚类：`cluster.SpectralClustering`


**5. 无监督学习算法——降维**

`sklearn.decomposition`模块包含了一系列无监督降维算法。

```python
from sklearn.decomposition import PCA
# 导入PCA库，设置主成分数量为3，n_components代表主成分数量
pca = PCA(n_components=3)
# 训练模型
pca.fit(X)
# 投影后各个特征维度的方差比例(这里是三个主成分)
print(pca.explained_variance_ratio_)
# 投影后的特征维度的方差
print(pca.explained_variance_)
```

### （六）评估模型

`sklearn.metrics`模块包含了一系列用于评价模型的评分函数、损失函数以及成对数据的距离度量函数。

```python
from sklearn.metrics import accuracy_score
accuracy_score(y_true, y_pred)
```

对于测试集而言，`y_test`即是`y_true`，大部分函数都必须包含真实值`y_true`和预测值`y_pred`。

**1. 回归模型评价**

- 平均绝对误差MAE：`metrics.mean_absolute_error()`
- 均方误差MSE：`metrics.mean_squared_error()`
- 决定系数R²：`metrics.r2_score()`

**2. 分类模型评价**

- 正确率：`metrics.accuracy_score()`
- 各类精确率：`metrics.precision_score()`
- F1值：`metrics.f1_score()`
- 对数损失或交叉熵损失：`metrics.log_loss()`
- 混淆矩阵：`metrics.confusion_matrix`
- 含多种评价的分类报告：`metrics.classification_report`

### （七）模型优化
**1. 交叉验证**

```python
from sklearn.model_selection import cross_val_score
clf = DecisionTreeClassifier(max_depth=5)
scores = cross_val_score(clf, X_train, y_train, cv=5, scoring=’f1_weighted’)
```
使用5折交叉验证对决策树模型进行评估，使用的评分函数为F1值。
sklearn提供了部分带交叉验证功能的模型类如`LassoCV`、`LogisticRegressionCV`等，这些类包含cv参数。

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/80df739fc1044da59af6c1acd330e893.png#pic_center =500x)
**2. 超参数调优⸺网格搜索**

```python
from sklearn.model_selection import GridSearchCV
from sklearn import svm
svc = svm.SVC()
params = {'kernel':['linear','rbf'],'C':[1, 10]}
grid_search = GridSearchCV(svc, params, cv=5)
grid_search.fit(X_train, y_train)
grid_search.best_params_
```

在参数网格上进行穷举搜索，方法简单但是搜索速度慢（超参数较多时），且不容易找到参数空间中的局部最优。

**3. 超参数调优⸺随机搜索**

```python
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import randint
svc = svm.SVC()
param_dist = {'kernel':['linear','rbf'], 'C':randint(1, 20)}
random_search = RandomizedSearchCV(svc, param_dist, n_iter=10)
random_search.fit(X_train, y_train)
random_search.best_params_
```

在参数子空间中进行随机搜索，选取空间中的100个点进行建模（可从scipy.stats常见分布如正态分布norm、均匀分布uniform中随机采样得到），时间耗费较少，更容易找到局部最优。

## 三、Scikit-learn案例

可参考：[Python数据分析实验四：数据分析综合应用开发](https://blog.csdn.net/Morse_Chen/article/details/139199909)

应用Scikit-Learn库中的逻辑回归对Scikit-Learn自带的乳腺癌（`from sklearn.datasets import load_breast_cancer`）数据集进行分类，并分别评估每种算法的分类性能。为了进一步提升算法的分类性能，能否尝试使用网格搜索和交叉验证找出每种算法较优的超参数。

```python
#加载数据集
from sklearn.datasets import load_breast_cancer
cancer=load_breast_cancer()

#对数据集进行预处理，实现数据标准化
from sklearn.preprocessing import StandardScaler
X=StandardScaler().fit_transform(cancer.data)
y=cancer.target

#将数据集划分为训练集和测试集(要求测试集占25%，随机状态random state设置为33)
from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25,random_state=33) 

#创建模型估计器estimator
from sklearn.linear_model import LogisticRegression
lgr=LogisticRegression()

#用训练集训练模型估计器estimator
lgr.fit(X_train,y_train)

#用模型估计器对测试集数据做预测
y_pred=lgr.predict(X_test)

#对模型估计器的学习效果进行评价
#最简单的评估方法：就是调用估计器的score(),该方法的两个参数要求是测试集的特征矩阵和标签向量
print("测试集的分类准确率为:",lgr.score(X_test,y_test))
print(" ")
from sklearn import metrics
#对于多分类问题，还可以使用metrics子包中的classification_report
print(metrics.classification_report(y_test,y_pred,target_names=cancer.target_names)) 

#网格搜索与交叉验证相结合的逻辑回归算法分类：
from sklearn.model_selection import GridSearchCV,KFold
params_lgr={'C':[0.01,0.1,1,10,100],'max_iter':[100,200,300],'solver':['liblinear','lbfgs']}
kf=KFold(n_splits=5,shuffle=False)

grid_search_lgr=GridSearchCV(lgr,params_lgr,cv=kf)
grid_search_lgr.fit(X_train,y_train)
grid_search_y_pred=grid_search_lgr.predict(X_test)
print("Accuracy:",grid_search_lgr.score(X_test,y_test))
print("best params:",grid_search_lgr.best_params_)
```

![在这里插入图片描述](https://i-blog.csdnimg.cn/direct/ee0d20c42172483cb085541c98f25204.png =600x)



> <font face='楷体' size=4>另外，推荐一个<font face='Times New Roman'
> size=4>Scikit-learn</font>学习网站：[<font face='Times New Roman'
> size=4>Scikit-learn</font>中文社区](https://scikit-learn.org.cn/)</font>

